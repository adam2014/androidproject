package com.adam.showme.layoutmanager;

import com.adam.showme.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/* 
* ImageAdapter class to be used in the GridView Layout
* 
* @author adan abdulrehman R00118124
*/
public class ImageAdapter extends BaseAdapter {
	private Context context;

	// references to our images
	private Integer[] imageIds = { R.drawable.brainymedium, R.drawable.clumsymedium,
			R.drawable.grouchymedium, R.drawable.gutsymedium, R.drawable.hackusmedium,
			R.drawable.papamedium, R.drawable.smurfettemedium, R.drawable.vanitymedium, 
			R.drawable.vexymedium };

	public ImageAdapter(Context context) {
		this.context = context;
	}

	public int getCount() {
		return imageIds.length;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ImageView imageView;
		
		if (convertView == null) { 
									
			imageView = new ImageView(context);
			imageView.setLayoutParams(new GridView.LayoutParams(128, 128));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);
			
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(imageIds[position]);
		return imageView;
	}
	
	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}
}