package com.adam.showme.layoutmanager;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/*
* Activity to show ImageView Layout
* 
* @author adan abdulrehman R00118124
*/

public class ImageViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_view);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_view, menu);
		return true;
	}

}
