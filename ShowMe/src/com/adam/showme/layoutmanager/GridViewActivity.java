package com.adam.showme.layoutmanager;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

/*
* Activity to show GridView Layout
* 
* @author adan abdulrehman R00118124
*/
public class GridViewActivity extends Activity {

	Context context = this;
	private String[] imageNames = { "brainy smurf", "clumsy smurf",
			"grouchy smurf", "gutsy smurf", "hackus smurf",
			"papa smurf", "smurfette smurf", "vanity smurf", "vexy smurf" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid_view);

		GridView gridView = (GridView) findViewById(R.id.gridview);
	
		// added with a custom adapter
	    gridView.setAdapter(new ImageAdapter(this));

	    gridView.setOnItemClickListener(new OnItemClickListener() {
	        
			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {
				Toast.makeText(context, "" + imageNames[position], Toast.LENGTH_SHORT).show();
			}
	    });

	    registerForContextMenu(gridView); 
	} 
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    
	    switch (item.getItemId()) {
	        case R.id.context_menu_item_hello:
	        	Toast.makeText(context, "hello there!", Toast.LENGTH_SHORT).show();
	            return true;
	        case R.id.context_menu_item_goodbye:
	        	Toast.makeText(context, "Goodbye smurferino!", Toast.LENGTH_SHORT).show();
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.grid_view, menu);
	}
}
