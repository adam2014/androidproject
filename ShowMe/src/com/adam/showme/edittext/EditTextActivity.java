package com.adam.showme.edittext;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/*
 * EditText Activity displays an example of the EditText Feature
 * 
 * @author adan abdulrehman R00118124
 */
public class EditTextActivity extends Activity {

	private final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_text);

		addNumericButtonListener();
		addSingleLineButtonListener();
	}

	public void addNumericButtonListener() {

		final EditText editText = (EditText) findViewById(R.id.editTextNumericInput);

		Button button = (Button) findViewById(R.id.editTextNumericInputBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(context, "EditText numeric input was: "
			                   + editText.getText().toString(),
						         Toast.LENGTH_LONG).show();
			}
		});
	}

	public void addSingleLineButtonListener() {

		final EditText editText = (EditText) findViewById(R.id.editTextSingleLineInput);

		Button button = (Button) findViewById(R.id.editTextSingleLineInputBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				Toast.makeText(
						context,
						"EditText Single line input was: "
								+ editText.getText().toString(),
						Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_text, menu);
		return true;
	}

}
