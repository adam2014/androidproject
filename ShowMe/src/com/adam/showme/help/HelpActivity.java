package com.adam.showme.help;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Help Activity displays help also used to demonstrate
 * Notification and Pending Intent.
 * 
 * @author adan abdulrehman R00118124
 */
public class HelpActivity extends Activity {

	public static final String HELP_FILE = "help.txt";
	private final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		loadShowMeFunctionalityFile();
	}

	public void loadShowMeFunctionalityFile() {

		try {
			InputStream is = getResources().getAssets().open(HELP_FILE);

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new DataInputStream(is)));

			TextView textView = (TextView) findViewById(R.id.text_view_help);
			String line = null;

			while ((line = reader.readLine()) != null) {
				textView.append(line);
				textView.append("\n");
			}

			is.close();

		} catch (Exception e) {
			// show a message to reflect error
			Toast.makeText(
					context,
					"Oops something went wrong in loading the file to display it in the TextView. "
							+ "Anyway, you see the TextView though so.",
					Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.help, menu);
		return true;
	}

}
