package com.adam.showme.musicplayer;

import java.io.IOException;

import com.adam.showme.R;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ToggleButton;

/*
* Activity to show How to play music
* 
* @author adan abdulrehman R00118124
*/
public class MusicPlayerActivity extends Activity implements OnPreparedListener {

	private static MediaPlayer player;

	//static Uri audio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music_player);

		preparePlayer();
	}
	
	public void preparePlayer() {
		player = MediaPlayer.create(this, R.raw.adele);

		//audio = Uri.parse("android.resource://" + getPackageName() + "/"+ R.raw.adele);

		try {
			player.setDataSource(getAssets().openFd("raw/adele.mp3")
					.getFileDescriptor());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		player.setOnPreparedListener(this);
		if (player.isPlaying()) {
			player.stop();
		}
	}

	public void play() {
		if (!player.isPlaying()) {
			try {
				player.prepare();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		if (player.isPlaying()) {
			player.stop();
		}
	}

	public void onToggleButtonClicked(View view) {

		// Is the toggle on?
		boolean on = ((ToggleButton) view).isChecked();

		if (on) {
			// start the music
			play();
		} else {

			stop();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music_player, menu);
		return true;
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		if (!player.isPlaying()) {
			player.start();
		}
	}

}
