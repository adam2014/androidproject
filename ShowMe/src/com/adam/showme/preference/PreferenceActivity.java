package com.adam.showme.preference;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*
* Activity to show the use of Preference files to save program data
* 
* @author adan abdulrehman R00118124
*/
public class PreferenceActivity extends Activity {

	public static final String NAME_SAVED = "nameSaved";
	private final Context context = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preference);
		
		addLoadPreferencesListener();
		addSavePreferenceListener();
	}

	private void addSavePreferenceListener() {
		Button button = (Button) findViewById(R.id.savePreferenceBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				saveToPreferenceFile();
			}
		});
		
	}

	private void saveToPreferenceFile() { 
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		Editor editor = prefs.edit();
		
		String preference = ((EditText) findViewById(R.id.preferenceInput)).getText().toString();
		
		if (preference.isEmpty()) {
			Toast.makeText(context,
					"Nothing entered. Cannot save empty value",
					Toast.LENGTH_LONG).show();
		}
		else {
			editor.putString(NAME_SAVED, preference);
			editor.commit();
		}
	}

	private void addLoadPreferencesListener() {
		Button button = (Button) findViewById(R.id.loadPreferencesBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				loadFromPreferenceFile();
			}
		});
	} 

	private void loadFromPreferenceFile() {
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);

		String preference = prefs.getString(NAME_SAVED, null);
		
		if (preference != null) {
			TextView view = (TextView) findViewById(R.id.savedPreferences);
			
			view.setText(null);
			view.append(preference);
		}
		else {
			Toast.makeText(context,
					"Nothing has been saved so far , so I cannit load it!",
					Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.preference, menu);
		return true;
	}

}
