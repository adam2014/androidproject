package com.adam.showme.fragment;

import java.util.ArrayList;

import com.adam.showme.R;
import com.adam.showme.edittext.EditTextActivity;
import com.adam.showme.help.HelpActivity;
import com.adam.showme.javaiosql.JavaIOSqlActivity;
import com.adam.showme.layoutmanager.FrameLayoutActivity;
import com.adam.showme.layoutmanager.GridViewActivity;
import com.adam.showme.layoutmanager.ImageViewActivity;
import com.adam.showme.musicplayer.MusicPlayerActivity;
import com.adam.showme.preference.PreferenceActivity;
import com.adam.showme.textview.TextViewActivity;
import com.adam.showme.uiwidgets.UiWidgetsActivity;

/*
 * Class representing an ArrayList of Features.
 * 
 * @author adan abdulrehman R00118124
 */
public class FeatureList extends ArrayList<Feature> {

	private static final long serialVersionUID = -7704406405762937800L;

	public FeatureList() {

		add(new Feature(R.drawable.papalarge,
				"Help/Notification & Pending Intent",
				R.drawable.notification_code, HelpActivity.class));

		add(new Feature(R.drawable.brainylarge, "TextView",
				R.drawable.textview_code, TextViewActivity.class));

		add(new Feature(R.drawable.clumsylarge, "EditText & Button",
				R.drawable.edittext_button_code, EditTextActivity.class));

		add(new Feature(R.drawable.hackuslarge, "ImageView",
				R.drawable.imageview_code, ImageViewActivity.class));

		add(new Feature(R.drawable.grouchylarge, "GridView with Images",
				R.drawable.gridview_s_code, GridViewActivity.class));

		add(new Feature(R.drawable.papalarge, "FrameLayout view",
				R.drawable.framelayout_code, FrameLayoutActivity.class));

		add(new Feature(R.drawable.smurfettelarge, "JavaIO & Sql",
				R.drawable.javaio_sql_code, JavaIOSqlActivity.class));

		add(new Feature(R.drawable.vanitylarge, "Preferences",
				R.drawable.preferences_code, PreferenceActivity.class));

		add(new Feature(R.drawable.vexylarge, "UI widgets",
				R.drawable.ui_widgets_code, UiWidgetsActivity.class));

		add(new Feature(R.drawable.hackuslarge, "Contextual Menus",
				R.drawable.context_menu_code, GridViewActivity.class));

		add(new Feature(R.drawable.hackuslarge, "Menu showing/Hiding",
				R.drawable.menu_hiding_code, UiWidgetsActivity.class));

		add(new Feature(R.drawable.grouchylarge, "Music",
				R.drawable.music_player_code, MusicPlayerActivity.class));
	}
}
