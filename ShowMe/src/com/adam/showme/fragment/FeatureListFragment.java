package com.adam.showme.fragment;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

/*
 * Fragment class to represent a List of fragments
 * The fragments are the features.
 * 
 * @author adan abdulrehman R00118124
 */
public class FeatureListFragment extends ListFragment {

	private OnFeatureItemClickListener onFeatureItemClickListener;

	public interface OnFeatureItemClickListener {
		public void onFeatureItemClicked(int position);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		if (onFeatureItemClickListener != null) {
			onFeatureItemClickListener.onFeatureItemClicked(position);
		}
	}

	public OnFeatureItemClickListener getOnFeatureItemClickListener() {
		return onFeatureItemClickListener;
	}

	public void setOnFeatureItemClickListener(
			OnFeatureItemClickListener onFeatureItemClickListener) {
		this.onFeatureItemClickListener = onFeatureItemClickListener;
	}

}
