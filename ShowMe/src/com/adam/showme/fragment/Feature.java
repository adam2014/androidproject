package com.adam.showme.fragment;

import java.io.Serializable;

/*
 * Class representing a Feature.
 * 
 * @author adan abdulrehman R00118124
 */
public class Feature implements Serializable {

	private static final long serialVersionUID = 3926905903573738036L;
	private int imageId;
	private String title;
	private int codeImageId;
	private Class<?> className;

	public Feature(int imageId, String title, int codeImageId,
			Class<?> className) {
		this.imageId = imageId;
		this.title = title;
		this.codeImageId = codeImageId;
		this.className = className;
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCodeImageId() {
		return codeImageId;
	}

	public void setCodeImageId(int codeImageId) {
		this.codeImageId = codeImageId;
	}

	public Class<?> getClassName() {
		return className;
	}

	public void setClassName(Class<?> className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return title;
	}

}
