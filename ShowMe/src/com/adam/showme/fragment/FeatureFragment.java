package com.adam.showme.fragment;

import com.adam.showme.R;
import com.adam.showme.activity.CodeViewActivity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Class representing Fragment of Features.
 * 
 * @author adan abdulrehman R00118124
 */

public class FeatureFragment extends Fragment implements OnClickListener {

	private Feature feature;
	public static final String DEBUG_TAG = "Adam";
	private Context context;

	public FeatureFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// get the feature passed from the previous activity
		Bundle dataBundle = getArguments();

		if (dataBundle != null) {

			// unwrap the dataBbundle and get the feature object
			this.feature = (Feature) dataBundle.get("FEATURE");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// get the feature passed from the previous activity
		Bundle dataBundle = getArguments();

		if (dataBundle != null) {

			// unwrap the dataBbundle and get the feature object
			this.feature = (Feature) dataBundle.get("FEATURE");
		}

		context = getActivity();

		View view = inflater.inflate(R.layout.fragment_feature, container,
				false);

		if (feature != null) {
			setFeature(feature, view);
		}

		Button demoButton = (Button) view.findViewById(R.id.demo_button);
		demoButton.setOnClickListener(this);

		Button codeButton = (Button) view.findViewById(R.id.code_button);
		codeButton.setOnClickListener(this);

		return view;
	}

	public void setFeature(Feature feature) {
		// get the view which was inflated from the xml from onCreate above
		View view = getView();
		setFeature(feature, view);
	}

	private void setFeature(Feature feature, View view) {

		// Initialize view
		ImageView imageView = (ImageView) view.findViewById(R.id.image);
		TextView featureTitleTextView = (TextView) view
				.findViewById(R.id.feature_title);

		// set the Drawable
		Drawable icon = getResources().getDrawable(feature.getImageId());

		imageView.setImageDrawable(icon);

		featureTitleTextView.setText(feature.getTitle());
	}

	@Override
	public void onPause() {
		// save any data you want top preserve
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.demo_button:

			// Trigger activity to view demo
			Intent demoIntent = new Intent(context, feature.getClassName());
			startActivity(demoIntent);
			return;

		case R.id.code_button:
			Toast.makeText(
					context,
					"The Code is best viewed horizontally. Flip the phone then!",
					Toast.LENGTH_SHORT).show();
			// Trigger activity to view code
			Intent codeIntent = new Intent(context, CodeViewActivity.class);

			Bundle dataBundle = new Bundle();
			dataBundle.putSerializable("FEATURE", feature);
			codeIntent.putExtra("FEATURE", dataBundle);
			startActivity(codeIntent);
			return;
		}
	}
}
