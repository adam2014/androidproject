package com.adam.showme.javaiosql;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List; 

import com.adam.showme.R;
import com.adam.showme.fragment.Feature;
import com.adam.showme.fragment.FeatureList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

/*
 * Activity to display the JavaIo and Sql features
 * 
 * @author adan abdulrehman R00118124
 */
public class JavaIOSqlActivity extends Activity {

	public static final String FEATURES_FILE = "features.txt";
	private final Context context = this;
	FeatureList featureList = new FeatureList();
	
	// a database instance to an sqlite database
	private Database db = new Database(context);

	List<String> features = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_java_iosql);

		loadInitialData();
		addloadFromFileListener();
		addloadFromDatabaseListener();
		addsaveToDatabaseListener();
		addsaveToFileListener();
	}

	private void loadInitialData() {
		
		// populate local features
		for (Feature feature : featureList) {
			features.add(feature.getTitle());
		}
				
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
				android.R.layout.simple_list_item_1, features);
		ListView list = (ListView) findViewById(R.id.database_list);
		list.setAdapter(null);
		list.setAdapter(adapter);
		
	}

	public void addloadFromFileListener() {

		Button button = (Button) findViewById(R.id.loadFromFileBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				loadFromFile();
			}
		});
	}

	public void addloadFromDatabaseListener() {

		Button button = (Button) findViewById(R.id.loadFromDatabaseBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				loadFromDatabase();
			}
		});
	}

	public void addsaveToDatabaseListener() {

		Button button = (Button) findViewById(R.id.saveToDatabaseBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				saveToDatabase();
			}
		});
	}

	public void addsaveToFileListener() {

		Button button = (Button) findViewById(R.id.saveToFileBtn);
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				saveToFile();
			}
		});
	}

	public void loadFromDatabase() {

		features.clear();

		// get the readings from the database
		features = db.getFeatures();

		// something went wrong with the db there
		if (features == null) {
			Toast.makeText(context, "Database says Nooooooooo",
					Toast.LENGTH_LONG).show();
			return;
		}

		// if the database was empty say so otherwise display the contents
		if (features.isEmpty()) {
			Toast.makeText(context, "Database says Noooooo it's empty",
					Toast.LENGTH_LONG).show();
			return;
		} else {

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
					android.R.layout.simple_list_item_1, features);
			ListView list = (ListView) findViewById(R.id.database_list);
			list.setAdapter(null);
			list.setAdapter(adapter);
			
			Toast.makeText(context, "Successfully loaded data from database",
					Toast.LENGTH_LONG).show();
		}
	}

	public void saveToDatabase() {

		// store the readings into the database
		boolean success = db.storeFeatures(features);

		// something went wrong with the db there
		if (!success) {
			Toast.makeText(context, "Database says Nooooooooo",
					Toast.LENGTH_LONG).show();
			return;
		} else {
			Toast.makeText(context, "Successfully saved data to database",
					Toast.LENGTH_LONG).show();
		}
	}

	public void loadFromFile() {

		if (features == null) {
			features = new ArrayList<String>();
		}

		features.clear();
		try {
			// Russian doll the streams
			FileInputStream fis = openFileInput(FEATURES_FILE);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new DataInputStream(fis)));

			String line = null;

			while ((line = reader.readLine()) != null) {
				features.add(line);
			}

			fis.close();

		} catch (Exception e) {
			// show a message to reflect error
			Toast.makeText(
					context,
					"Oops something went wrong in loading the file to display it in the TextView. "
							+ "Anyway, you see the TextView though so.",
					Toast.LENGTH_LONG).show();
		}

		// No data in the file
		if (features.isEmpty()) {
			Toast.makeText(context, "No data was loaded. File was empty",
					Toast.LENGTH_LONG).show();
			return;
		} else {

			// populate the listView with data from file
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
					android.R.layout.simple_list_item_1, features);
			ListView list = (ListView) findViewById(R.id.database_list);
			list.setAdapter(null);
			list.setAdapter(adapter);
			Toast.makeText(context,
					"Successfully loaded  data from file",
					Toast.LENGTH_LONG).show();
		}
	}

	public void saveToFile() {
		if (features == null) {
			features = new ArrayList<String>();
		}

		if (features.isEmpty()) {
			Toast.makeText(context,
					"There is nothing to save. Try loading data first!",
					Toast.LENGTH_LONG).show();
			return;
		} else {

			try {
				FileOutputStream fos = openFileOutput(FEATURES_FILE,
						MODE_PRIVATE);
				//InputStream is = getResources().getAssets().open(FEATURES_FILE);
				
				for (String feature : features ) {
					fos.write(feature.getBytes());
					fos.write("\n".getBytes());
				}
				
				fos.close();
				Toast.makeText(context,
						"Successfully saved  data to file",
						Toast.LENGTH_LONG).show();
				
			} catch (Exception e) {
				Toast.makeText(context,
						"Some error occured. Could not save to file.",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.java_iosql, menu);
		return true;
	}

}
