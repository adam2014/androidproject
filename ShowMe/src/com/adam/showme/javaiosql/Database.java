package com.adam.showme.javaiosql;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*
 * Service class to access an SQLite database that is 
 * used to store String values.
 * 
 * @author adan abdulrehman R00118124
 */

public class Database extends SQLiteOpenHelper {

	public static final String DEBUG_TAG = "Adam";

	private static final String FEATURES_TABLE = "features";
	private static final String COLUMN_ID = "id";
	private static final String COLUMN_FEATURE = "feature";

	public Database(Context context) {
		super(context, "showme.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// sql to create the table that will hold the features
		String sql = String.format("create table %s (%s INTEGER PRIMARY KEY, "
				+ "%s VARCHAR NOT NULL )", FEATURES_TABLE, COLUMN_ID,
				COLUMN_FEATURE);

		// create the table by executing the sql
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	// store a value into the database
	public boolean storeFeature(String feature) {
		try {
			// create a database instance which we can write the reading into
			SQLiteDatabase db = getWritableDatabase();

			// if you wanted to clear the table of all the values
			// db.delete(FEATURES_TABLE, null, null);

			ContentValues values = new ContentValues();
			values.put(COLUMN_FEATURE, feature);
			db.insert(FEATURES_TABLE, null, values);

			db.close();
			return true;

		} catch (SQLException mSQLException) {
			Log.d(DEBUG_TAG, "Exception in getting the features");
			return false;
		}

	}

	public List<String> getFeatures() {
		List<String> features = new ArrayList<String>();
		SQLiteDatabase db = getReadableDatabase();

		String sql = String.format("SELECT %s FROM %s", COLUMN_FEATURE,
				FEATURES_TABLE);

		try {
			Cursor cursor = db.rawQuery(sql, null);

			while (cursor.moveToNext()) {

				String feature = cursor.getString(0);
				features.add(feature);
			}

			db.close();
		} catch (SQLException mSQLException) {
			Log.d(DEBUG_TAG, "Exception in getting the features");
			return null;
		}

		return features;
	}

	// store the list of values to the database
	public boolean storeFeatures(List<String> features) {

		for (String feature : features) {
			if (!storeFeature(feature)) {
				return false;
			}
		}

		return true;
	}
}
