package com.adam.showme.textview;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

/*
 * Activity to show the TextView
 * 
 * @author adan abdulrehman R00118124
 */
public class TextViewActivity extends Activity {

	public static final String SHOW_ME_FUNCTIONALITY_FILE = "showmefunctionality.txt";
	private final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text_view);

		loadShowMeFunctionalityFile();
	}

	public void loadShowMeFunctionalityFile() {

		try {
			InputStream is = getResources().getAssets().open(
					SHOW_ME_FUNCTIONALITY_FILE);

			// FileInputStream fis = openFileInput(SHOW_ME_FUNCTIONALITY_FILE);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new DataInputStream(is)));

			TextView textView = (TextView) findViewById(R.id.text_view_demo);
			String line = null;

			while ((line = reader.readLine()) != null) {
				textView.append(line);
				textView.append("\n");
			}

			is.close();

		} catch (Exception e) {
			// show a message to reflect error
			Toast.makeText(
					context,
					"Oops something went wrong in loading the file to display it in the TextView. "
							+ "Anyway, you see the TextView though so.",
					Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.text_view, menu);
		return true;
	}

}
