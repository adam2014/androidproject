package com.adam.showme.uiwidgets;

import com.adam.showme.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/*
 * Activity to show the use of UI widgets
 * Widgets are spinner, CheckBox, ImageButton,
 * ToggleButton, RadioButton and also shows
 * Hiding and showing of menus depending on
 * the App state
 * 
 * @author adan abdulrehman R00118124
 */
public class UiWidgetsActivity extends Activity implements
		OnItemSelectedListener {

	private final Context context = this;
	private static int clickTimes = 0;
	private static boolean appStateShowMenu = true;
	Menu menu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ui_widgets);

		Spinner spinner = (Spinner) findViewById(R.id.ui_spinner);

		// Create an ArrayAdapter using the string array and a default spinner
		// layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.smurfs_array,
				android.R.layout.simple_spinner_item);

		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);

		// specify the OnItemSelectedListener interface implementation by
		// calling
		// setOnItemSelectedListener()
		spinner.setOnItemSelectedListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		this.menu = menu;

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ui_widgets, menu);
		return true;
	}

	public void onCheckboxClicked(View view) {

		TextView textView = (TextView) findViewById(R.id.ui_text_view);

		boolean checked = ((CheckBox) view).isChecked();
		textView.setText(null);

		// Is the view now checked?
		if (checked) {
			textView.append("You checked checkbox");
		} else {
			textView.append("You unchecked checkbox");
		}

	}

	public void onImageButtonClicked(View view) {

		clickTimes++;
		TextView textView = (TextView) findViewById(R.id.ui_text_view);

		textView.setText(null);
		textView.append("You Clicked the image button " + clickTimes + " times");
	}

	public void onToggleButtonClicked(View view) {

		TextView textView = (TextView) findViewById(R.id.ui_text_view);
		textView.setText(null);

		// Is the toggle on?
		boolean on = ((ToggleButton) view).isChecked();

		if (on) {
			textView.append("ToggleButton - Music on");
		} else {
			textView.append("ToggleButton - Music off");
		}
	}

	public void onRadioButtonClicked(View view) {

		TextView textView = (TextView) findViewById(R.id.ui_text_view);
		textView.setText(null);

		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();

		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.ui_radioButton_love:
			if (checked)
				textView.append("You selected from Radio button I love you");
			break;
		case R.id.ui_radioButton_hate:
			if (checked)
				textView.append("You selected from Radio button I hate you");
			break;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		String spinnerSelection = (String) parent.getItemAtPosition(pos);
		TextView textView = (TextView) findViewById(R.id.ui_text_view);

		textView.setText(null);
		textView.append("You selected from the spinner " + spinnerSelection);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	@Override
	public boolean onMenuItemSelected(int menuId, MenuItem item) {
		
		switch (item.getItemId()) {

		case R.id.item_menu_hiding_show:

			if (appStateShowMenu) {
				// menu should be hidden now
				item.setTitle("Show sorry");
				appStateShowMenu = false;
				MenuItem itemToHide = menu
						.findItem(R.id.item_menu_hiding_sorry);

				itemToHide.setVisible(appStateShowMenu);
				Toast.makeText(context, "Hidden menu item entitled sorry!",
						Toast.LENGTH_SHORT).show();

			} else {
				// menu should be shown now
				item.setTitle("Hide sorry");
				appStateShowMenu = true;
				MenuItem itemToHide = menu
						.findItem(R.id.item_menu_hiding_sorry);

				itemToHide.setVisible(appStateShowMenu);
				Toast.makeText(context, "Shown menu item entitled sorry!",
						Toast.LENGTH_SHORT).show();
			}

			return true;

		case R.id.item_menu_hiding_sorry:

			Toast.makeText(context, "Sorry!", Toast.LENGTH_SHORT).show();
			return true;

		case R.id.item_menu_hiding_goodbye:

			Toast.makeText(context, "Goodbye!", Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onMenuItemSelected(menuId, item);
		}

	}
}
