package com.adam.showme.activity;

import com.adam.showme.R;
import com.adam.showme.fragment.Feature;
import com.adam.showme.fragment.FeatureFragment;
import com.adam.showme.fragment.FeatureList;
import com.adam.showme.fragment.FeatureListFragment;
import com.adam.showme.help.HelpActivity;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

/*
 * Main Activity containing Menus, Deployment of Notification
 * and Management of Fragments.
 * 
 * @author adan abdulrehman R00118124
 */

public class MainActivity extends Activity {

	public static final String DEBUG_TAG = "Adam";
	private final Context context = this;

	private FeatureList features = new FeatureList();
	private FeatureListFragment featureListFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState != null) {
			return;
		}

		// Fragments
		// get the fragment FeatureListFragment
		featureListFragment = (FeatureListFragment) getFragmentManager()
				.findFragmentById(R.id.fragment_feature_list);
		// initialize fragment data
		ArrayAdapter<Feature> adapter = new ArrayAdapter<Feature>(context,
				R.layout.feature_list_item, features);

		featureListFragment.setListAdapter(adapter);

		// Observer pattern used here as we don't want to clutter
		// the application and make it look like a plate of spaghetti
		// listen to feature selection and update feature appropriately
		featureListFragment
				.setOnFeatureItemClickListener(new FeatureListFragment.OnFeatureItemClickListener() {

					@Override
					public void onFeatureItemClicked(int position) {

						// Features extends ArrayList so we can get the feature
						// at specified position
						Feature feature = features.get(position);

						FeatureFragment featureFragment = new FeatureFragment();

						Bundle dataBundle = new Bundle();
						dataBundle.putSerializable("FEATURE", feature);
						featureFragment.setArguments(dataBundle);

						// lets get the fragment transaction so we can muck
						// about with the fragments and swap them around
						FragmentTransaction transaction = getFragmentManager()
								.beginTransaction();

						// swap the fragment for the appropriate one
						transaction.replace(R.id.fragment_feature_list,
								featureFragment);

						// add the old state of the application to a stack which
						// we can go back to so when a user clicks the back button they do not
						// come out of the app the back buttons acts on activity level not
						// fragment level
						transaction.addToBackStack(null);

						transaction.commit();
					}
				});

		// prepare intent which is triggered if the notification is selected
		Intent intent = new Intent(this, HelpActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// build notification
		Notification.Builder mBuilder = new Notification.Builder(this)
				.setSmallIcon(R.drawable.hackus)
				.setContentTitle("Help notification")
				.setContentText("Help for the showMe App!")
				.setContentIntent(pIntent);

		// Gets an instance of the NotificationManager service
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Sets an ID for the notification
		int mNotificationId = 001;

		// Builds the notification and issues it.
		notificationManager.notify(mNotificationId, mBuilder.getNotification());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.item_menu_forgive_me:
			// Make an alert dialog to inform user that fields are empty
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);

			alertDialogBuilder
					.setMessage("I ask for your forgiveness. What will you do?");

			alertDialogBuilder.setPositiveButton("Forgive",
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// add code to handle iconic fragment code display
							// TODO Auto-generated method stub
							Toast.makeText(context,
									"Thank you and I love you so much!",
									Toast.LENGTH_LONG).show();
						}
					});

			alertDialogBuilder.setNegativeButton("Don't bother",
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							// show a message to reflect selection
							Toast.makeText(context,
									"Sod off you evil so and so",
									Toast.LENGTH_LONG).show();
						}
					});

			AlertDialog dfrDialog = alertDialogBuilder.create();
			dfrDialog.show();

			Toast.makeText(context, "Hello there!", Toast.LENGTH_SHORT).show();
			return true;

		case R.id.item_menu_hello:

			Toast.makeText(context, "Hello there!", Toast.LENGTH_SHORT).show();
			return true;

		case R.id.item_menu_goodbye:

			Toast.makeText(context, "Goodbye Smurferino!", Toast.LENGTH_SHORT)
					.show();
			return true;

		case R.id.item_menu_sorry:

			Toast.makeText(context, "Sorry for not being there for you!",
					Toast.LENGTH_SHORT).show();
			return true;

		default:
			return super.onMenuItemSelected(featureId, item);
		}

	}
}
