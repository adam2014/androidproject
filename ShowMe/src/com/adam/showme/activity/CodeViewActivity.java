package com.adam.showme.activity;

import com.adam.showme.R;
import com.adam.showme.fragment.Feature;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.widget.ImageView;

/*
 * Activity to display the feature's code.
 * 
 * @author adan abdulrehman R00118124
 */
public class CodeViewActivity extends Activity {

	private Feature feature;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_code_view);

		// get the feature passed from the previous activity
		// unwrap the bundle and get the calculation
		Bundle extras = getIntent().getExtras();

		if (extras != null) {

			// unwrap the extras bundle
			Bundle dataBundle = (Bundle) extras.get("FEATURE");

			if (dataBundle != null) {
				// unwrap the dataBbundle and get the calculation object
				feature = (Feature) dataBundle.get("FEATURE");
			}
		}

		// Initialize view
		final ImageView imageView = (ImageView) findViewById(R.id.codeImageView);

		// set the Drawable
		Drawable icon = getResources().getDrawable(feature.getCodeImageId());

		imageView.setImageDrawable(icon);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.code_view, menu);
		return true;
	}

}
